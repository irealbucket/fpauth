//
//  Button_Custom.swift
//  FPAuth
//
//  Created by arima on 2016/07/11.
//  Copyright © 2016年 arm. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class Button_Custom: UIButton {
    @IBInspectable var textColor: UIColor?
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
}