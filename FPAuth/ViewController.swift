//
//  ViewController.swift
//  FPAuth
//
//  Created by arima on 2016/07/06.
//  Copyright © 2016年 arm. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var TouchIdButtomPressed: UIButton!
    @IBOutlet weak var Button1: Button_Custom!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func TouchIdButtomPressed(sender: AnyObject) {
        
        let context = LAContext()
        var error :NSError?
        let localizedReason = "指の紋様が欲しいです。"
        
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error){
            //TocuhIDに対応している場合
            context.evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: localizedReason, reply: {
                success, error in
    
                if success {
                    NSLog("認証成功")
                    self.performSegueWithIdentifier("moveList",sender: nil)
                } else {
                    NSLog("認証失敗")
                }
            })
            
        }else{
            //TocuhIDに対応していない場合
            NSLog("TourchIDに対応してない")
            //実機でやらない場合のみコメント外す
            //self.performSegueWithIdentifier("moveList",sender: nil)
        }
    }
}

