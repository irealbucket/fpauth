//
//  EnterForm.swift
//  FPAuth
//
//  Created by arima on 2016/08/27.
//  Copyright © 2016年 arm. All rights reserved.
//

import Foundation
import UIKit

class EnterForm: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var NameText: UITextField!
    @IBOutlet weak var AccountText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!
    
    //キーボードが出ている状態で、キーボード以外をタップしたらキーボードを閉じる
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //非表示にする。
        if(NameText.isFirstResponder()){
            NameText.resignFirstResponder()
        }else if(AccountText.isFirstResponder()){
            AccountText.resignFirstResponder()
        }else if(PasswordText.isFirstResponder()){
            PasswordText.resignFirstResponder()
        }
    }
}

